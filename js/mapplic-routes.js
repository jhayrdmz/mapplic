jQuery(document).ready(function($) {
	var map = $('.mapplic-routes'),
		self = map.data('mapplic'),
		wayfinding = null;

	var buildFloors = function() {
		wayfinding = new Wayfinding().init();
		map.on('svgloaded', function(e, svg, id) {
			wayfinding.build($('[id^=route]', svg), id);
		});
	}

	if (self) buildFloors();
	else {
		map.on('mapready', function(e, s) {
			self = s;
			buildFloors();
		});
	}

	// Wayfinding
	function Wayfinding() {
		this.waypoints = [];
		this.element = null;
		this.path = null;
		this.el = null;
		this.submit = null;
		this.button = null;
		this.color = '#f23543';
		this.strokewidth = 2;
		this.timeouts = [];

		this.init = function() {
			this.el = this.markup();
			self.container.el.append(this.el);

			return this;
		}

		this.markup = function() {
			var s = this;

			// Button
			this.button = $('<a>-></a>').attr('href', '#').addClass('mapplic-ui-button mapplic-routes-button').appendTo(self.container.el);
			this.button.on('click touchstart', function(e) {
				e.preventDefault();
				s.el.fadeIn(100);
				$(this).fadeOut(100);
			});

			// Panel
			this.el = $('<div></div>').addClass('mapplic-routes-panel');
			var close = $('<div></div>').addClass('mapplic-routes-close').appendTo(this.el);

			var fromselect = $('<div></div').addClass('mapplic-routes-select').appendTo(this.el);
			$('<small>From:</small>').appendTo(fromselect);
			$('<div></div>').appendTo(fromselect);

			var toselect = $('<div></div>').addClass('mapplic-routes-select').appendTo(this.el);
			$('<small>To:</small>').appendTo(toselect);
			$('<div></div').appendTo(toselect);

			var swap = $('<div></div>').addClass('mapplic-routes-swap').appendTo(this.el);
			swap.on('click', function() {
				$(this).toggleClass('rotate');

				var from = $('> div', fromselect);
				$('> div', toselect).appendTo(fromselect);
				from.appendTo(toselect);

			});

			$(document).on('click', '.mapplic-routes-select', function() {
				$('.active', s.el).removeClass('active');
				$(this).addClass('active');
			});

			close.on('click touchstart', function() {
				s.clear();
				$('.mapplic-routes-select > div', self.el).empty();

				s.hidePanel(s);
			});

			this.submit = $('<a></a>').attr('href', '#').text('Go').addClass('mapplic-routes-submit').attr('type', 'submit').appendTo(this.el);
			this.submit.on('click touchstart', function(e) {
				e.preventDefault();
				
				var f = $('.mapplic-routes-loc', fromselect).data('location'),
					t = $('.mapplic-routes-loc', toselect).data('location');

				$('.active', s.el).removeClass('active');

				if (f && t) wayfinding.showPath(f, t);
				else if (!f) fromselect.addClass('active');
				else if (!t) toselect.addClass('active');
				self.hideLocation();
			});

			$(document).on('click touchstart', '.mapplic-routes-icon', function() {
				var loc = $('<div></div>').addClass('mapplic-routes-loc').text($(this).siblings('a').children('h4').text()).attr('data-location', $(this).closest('li').attr('data-location'));
				$('> div', toselect).empty().append(loc);
				fromselect.trigger('click');

				s.showPanel(s);
			});

			self.el.on('locationopened', function(e, location) {
				var loc = $('<div></div>').addClass('mapplic-routes-loc').text(location.title).attr('data-location', location.id);
				$('.mapplic-routes-select.active > div', s.el).empty().append(loc);
			});

			return this.el;
		}

		this.showPanel = function(s) {
			s.el.fadeIn(100);
			s.button.fadeOut(100);
		}

		this.hidePanel = function(s) {
			s.el.fadeOut(100);
			s.button.fadeIn(100);

			$('.active', s.el).removeClass('active');
		}

		this.build = function(routes, fid) {
			var s = this;
			this.element = routes;
			$('> *', routes).each(function() {
				var id = $(this).attr('id');
				switch (this.tagName) {
					case 'line':
						var a = s.addPoint($(this).attr('x1'), $(this).attr('y1'), id, routes, fid),
							b = s.addPoint($(this).attr('x2'), $(this).attr('y2'), id, routes, fid);
							val = s.distance(a, b);
						s.linkPoint(a, b, val);
						s.linkPoint(b, a, val);
						break;
					case 'polygon':
					case 'polyline':
						var pairs = $(this).attr('points').replace(/\s\s+/g, ' ').trim().split(' ');
						var first = null,
							prev = null;
						for (var i = 0; i < pairs.length; i++) {
							var pair = pairs[i].split(','),
								point = s.addPoint(pair[0], pair[1], id, routes, fid);

							if (prev) {
								var val = s.distance(point, prev);
								s.linkPoint(point, prev, val);
								s.linkPoint(prev, point, val);
							}
							else first = point;
							prev = point;
						}

						if (this.tagName == 'polygon') {
							var val = s.distance(first, point);
							s.linkPoint(point, first, val);
							s.linkPoint(first, point, val);
						}
						break;
					default:
						console.log('Warning: Invalid element in routes: ' + this.tagName + '. Valid types are line, polyline and polygon.');
				}
			});

			if (routes.length > 0) {
				for (var i = 0; i < this.waypoints.length; i++) {
					this.drawCircle(this.waypoints[i], '#b7a6bd');

					// Linking floors
					if (this.waypoints[i].id && (this.waypoints[i].id.indexOf('pf-') == 0)) {
						var g = this.waypoints[i].id.split('-').slice(0,2).join('-');
						for (var j = i+1; j < this.waypoints.length; j++) {
							if (this.waypoints[j].id && this.waypoints[j].id.indexOf(g) == 0) {
								s.linkPoint(this.waypoints[i], this.waypoints[j], 20);
								s.linkPoint(this.waypoints[j], this.waypoints[i], 20);
							}
						}
					}
				}
			}

		}

		this.showPath = function(a, b) {
			var wpa = this.getPoint('p-' + a),
				wpb = this.getPoint('p-' + b),
				path = this.shortestPath(wpa, wpb),
				start = 0,
				dist = 0;

			this.clear();

			// Multifloor support
			for (var i = 1; i < path.length; i++) {
				if (path[i-1].fid != path[i].fid) {
					this.showSubPath(path.slice(start, i), path[i].dist - dist, dist, path[start].fid);
					dist = path[i].dist;
					start = i;
				}
			}
			this.showSubPath(path.slice(start, path.length), wpb.dist - dist, dist, path[start].fid); // Last or only floor
		}

		this.showSubPath = function(subpath, dist, dur, fid) {
			var s = this;
			var t = setTimeout(function() {
				self.switchLevel(fid);
				var path = s.drawPath(subpath, dist),
					bbox = path[0].getBBox();
				self.moveTo((bbox.x + bbox.width/2) / self.contentWidth, (bbox.y + bbox.height/2) / self.contentHeight, s.bboxZoom(bbox));
			}, dur * 10 + s.timeouts.length * 600 /* delay */);
			s.timeouts.push(t);
		}

		this.bboxZoom = function(bbox) {
			var padding = 40,
				wr = self.container.el.width() / (bbox.width + padding),
				hr = self.container.el.height() / (bbox.height + padding);

			return Math.min(wr, hr);
		}

		this.drawCircle = function(wp, color) {
			color = typeof color !== 'undefined' ? color : 'red';
			var circle = $(this.svg('circle')).attr('cx', wp.x)
				.attr('cy', wp.y)
				.attr('r', 2)
				.attr('fill', color)
				.attr('stroke', 'none')
				.appendTo(wp.floor);
		}

		this.drawPath = function(list, dist) {
			var d = 'M ' + list[0].x + ',' + list[0].y;
			for (var i = 0; i < list.length; i++) d += ' L' + list[i].x + ',' + list[i].y;
			
			this.path = $(this.svg('path'))
				.attr('class', 'mapplic-routes-path')
				.attr('stroke', this.color)
				.attr('stroke-width', this.strokewidth)
				.attr('stroke-linecap', 'round')
				.attr('stroke-linejoin', 'round')
				.attr('d', d)
				.attr('fill', 'none')
				.css('display', 'block')
				.appendTo(list[0].floor.parent());

			// animation
			var p = this.path.get(0),
				length = p.getTotalLength();
			
			p.style.strokeDasharray = length + ' ' + length;
			p.style.strokeDashoffset = length;
			p.getBoundingClientRect();
			p.style.transition = p.style.WebkitTransition = 'stroke-dashoffset ' + dist/100 + 's ease-in-out';
			p.style.strokeDashoffset = '0';

			return this.path;
		}

		this.clear = function() {
			$('.mapplic-routes-path', map).remove();
			for (var i = 0; i < this.timeouts.length; i++) { clearTimeout(this.timeouts[i]); }
			this.timeouts = [];
		}

		this.shortestPath = function(a, b) {
			for (var i = 0; i < this.waypoints.length; i++) {
				this.waypoints[i].dist = Number.POSITIVE_INFINITY;
				this.waypoints[i].prev = undefined;
			}

			// Dijkstra
			a.dist = 0;
			var q = this.waypoints.slice();

			while (q.length > 0) {
				var min = Number.POSITIVE_INFINITY,
					u = 0;
				for (var i = 0; i < q.length; i++) {
					if (q[i].dist < min) {
						u = i;
						min = q[i].dist;
					}
				}
				var p = q[u];
				q.splice(u, 1);
				for (var i = 0; i < p.n.length; i++) {
					var alt = p.dist + p.n[i].val;
					if (alt < p.n[i].to.dist) {
						p.n[i].to.dist = alt;
						p.n[i].to.prev = p;
					}
				}
			}

			var target = b,
				path = [target];
			while (target.prev !== undefined) {
				target = target.prev;
				path.unshift(target);
			}
			return path;
		}

		this.addPoint = function(x, y, id, floor, fid) {
			var point = this.pointExists(this.waypoints, x, y);
			if (!point) {
				this.waypoints.push({
					id: id,
					x: x,
					y: y,
					floor: floor,
					fid: fid,
					n: []
				});
				point = this.waypoints[this.waypoints.length - 1];
			
				// List button
				if (id) $('<div></div>').addClass('mapplic-routes-icon').appendTo($('.mapplic-list-location[data-location=' + id.replace('p-', '') + ']', self.el));
			}

			return point;
		}

		this.getPoint = function(id) {
			for (var i = 0; i < this.waypoints.length; i++) {
				if (this.waypoints[i].id == id) return this.waypoints[i];
			}
			return null;
		}

		this.linkPoint = function(a, b, val) {
			val = typeof val !== 'undefined' ? val : 0;
			if (!this.pointExists(a.n, b.x, b.y)) {
				a.n.push({
					to: b,
					val: val
				});
			}
		}

		this.pointExists = function(list, x, y) {
			for (var i = 0; i < list.length; i++) {
				if ((list[i].x == parseFloat(x)) && (list[i].y == parseFloat(y))) {
					return list[i];
				}
			}
			return null;
		}

		this.distance = function(a, b) {
			return Math.sqrt(Math.pow((a.x - b.x), 2) + Math.pow((a.y - b.y), 2));
		}

		this.svg = function(tag) {
			return document.createElementNS('http://www.w3.org/2000/svg', tag);
		}
	}

});